Attribute VB_Name = "Module1"
Public Sub inputbox_selection()
    sinput = Application.InputBox("Please select colorscheme 1 to 3" & vbLf & vbLf & "1. White-red-black-blue" & vbLf & "2. White-green-black-violet" & vbLf & "3. White-blue-black-orange", "Enter a value", 1)
    If sinput = False Then
        Exit Sub
    Else
        Select Case sinput
            Case 1
                color_scheme = 1   'change to your desired action for each case
            Case 2
                color_scheme = 2
            Case 3
                color_scheme = 3
            Case Else
                dummy = MsgBox("wrong input", vbCritical)
        End Select
    End If
End Sub

Sub graph()
'
' graph Macro
'
'
'    MsgBox "Please select data range"
    ' select range for the data
    Dim rng1 As Range
    Dim rng2 As Range
    Dim rng3 As Range
    Dim rng4 As Range
    Dim rng1_errorbars As Range
    Dim rng2_errorbars As Range
    Dim rng3_errorbars As Range
    Dim rng4_errorbars As Range
    Dim rng_names As Range
    Dim color_scheme As Integer
    Dim colors As Variant
    Dim i As Integer
    colors = Array(vbWhite, vbRed, vbBlack, vbBlue)
    
    Set rng_names = Application.InputBox("Select a range for names", "Obtain Range Object", Type:=8)
    
    Set rng1 = Application.InputBox("Select a range for Serie1", "Obtain Range Object", Type:=8)
    ActiveSheet.Shapes.AddChart2(201, xlColumnClustered).Select
    ActiveChart.SetSourceData Source:=rng1
    Set rng2 = Application.InputBox("Select a range for Serie2", "Obtain Range Object", Type:=8)
    ActiveChart.SeriesCollection.NewSeries
    ActiveChart.FullSeriesCollection(1).Name = rng_names(1)
    ActiveChart.FullSeriesCollection(2).Values = rng2
    ActiveChart.FullSeriesCollection(2).Name = rng_names(2)
    Set rng3 = Application.InputBox("Select a range for Serie3", "Obtain Range Object", Type:=8)
    ActiveChart.SeriesCollection.NewSeries
    ActiveChart.FullSeriesCollection(3).Values = rng3
    ActiveChart.FullSeriesCollection(3).Name = rng_names(3)
    Set rng4 = Application.InputBox("Select a range for Serie4", "Obtain Range Object", Type:=8)
    ActiveChart.SeriesCollection.NewSeries
    ActiveChart.FullSeriesCollection(4).Values = rng4
    ActiveChart.FullSeriesCollection(4).Name = rng_names(4)

    
    Set rng1_errorbars = rng1.Cells.Offset(0, 1)
    Set rng2_errorbars = rng2.Cells.Offset(0, 1)
    Set rng3_errorbars = rng3.Cells.Offset(0, 1)
    Set rng4_errorbars = rng4.Cells.Offset(0, 1)
    
    ActiveChart.FullSeriesCollection(1).HasErrorBars = True
    ActiveChart.FullSeriesCollection(1).ErrorBar Direction:=xlY, Include:=xlPlusValues, Type:=xlCustom, Amount:=rng1_errorbars
    ActiveChart.FullSeriesCollection(2).HasErrorBars = True
    ActiveChart.FullSeriesCollection(2).ErrorBar Direction:=xlY, Include:=xlPlusValues, Type:=xlCustom, Amount:=rng2_errorbars
    ActiveChart.FullSeriesCollection(3).HasErrorBars = True
    ActiveChart.FullSeriesCollection(3).ErrorBar Direction:=xlY, Include:=xlPlusValues, Type:=xlCustom, Amount:=rng3_errorbars
    ActiveChart.FullSeriesCollection(4).HasErrorBars = True
    ActiveChart.FullSeriesCollection(4).ErrorBar Direction:=xlY, Include:=xlPlusValues, Type:=xlCustom, Amount:=rng4_errorbars

    ActiveChart.ChartTitle.Select
    Selection.Delete
    ActiveChart.Axes(xlValue).MajorGridlines.Select
    Selection.Delete
    ActiveSheet.ChartObjects(1).Activate
    
    With ActiveChart.ChartArea.Format.TextFrame2.TextRange.Font
        .Name = "Helvetica"
        .Size = 8
        .Fill.ForeColor.RGB = RGB(0, 0, 0)
        .Fill.Transparency = 0
        .Fill.Solid
    End With
    
    With ActiveChart.ChartArea.Format.Line
        .Visible = msoFalse
    End With
    
    ActiveChart.Axes(xlValue).MajorTickMark = xlOutside
    With ActiveChart.Axes(xlValue).Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorText1
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
        .Weight = 0.5
    End With
    
    With ActiveChart.Axes(xlCategory).Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorText1
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
        .Weight = 0.5
    End With
    
    With ActiveChart.Axes(xlCategory)
        .TickLabelPosition = xlNone
    End With
    
    'Call inputbox_selection
    i = 0
    For Each fsc In ActiveChart.FullSeriesCollection
        With fsc.Format.Line
            .Visible = msoTrue
            .ForeColor.ObjectThemeColor = msoThemeColorText1
            .ForeColor.TintAndShade = 0
            .ForeColor.Brightness = 0
            .Transparency = 0
        End With
        With fsc.ErrorBars.Format.Line
            .Visible = msoTrue
            .ForeColor.ObjectThemeColor = msoThemeColorText1
            .ForeColor.TintAndShade = 0
            .ForeColor.Brightness = 0
            .Transparency = 0
            .Weight = 0.5
        End With
        With fsc.Format.Fill
            If i = 0 Then
                .ForeColor.RGB = RGB(255, 255, 255) 'white
            ElseIf i = 1 Then
                .ForeColor.RGB = RGB(0, 176, 80) 'green
                '.ForeColor.RGB = RGB(192, 0, 0) 'red
                '.ForeColor.RGB = RGB(244, 177, 131) 'light orange
            ElseIf i = 2 Then
                .ForeColor.RGB = RGB(0, 0, 0) 'black
            ElseIf i = 3 Then
                .ForeColor.RGB = RGB(112, 48, 160) 'violet
                '.ForeColor.RGB = RGB(0, 112, 192) 'blue
                '.ForeColor.RGB = RGB(0, 176, 240) 'light blue
            End If
        End With
        i = i + 1
    Next fsc
    
    ActiveChart.ChartGroups(1).Overlap = 0
    ActiveChart.ChartGroups(1).GapWidth = 20
End Sub
