Sub overlay()
'
' overlay Macro
'
' crazy thing to do overlay barplots in Excel. though ugly, in principle, it works.
' ask to select area, count data series, pre-last is mean, last is error bars
' area - transparent, no border, no grid lines, no title
' y axis - line 0.5pt, black, ticks outside, label font Helvetica 8pt
' x axis - line 0.5pt, black, ticks outside, no labels
' markers - line 0.25pt, size 3, circle, no line for data series, fill black
' error bars - line 0.5pt, black
' set colors manually for each bar

    Application.MacroOptions Macro:="overlay", Description:="", ShortcutKey:= _
        "k"
    Application.Goto Reference:="overlay"
    
    Dim rng1 As Range
    Set rng1 = Application.InputBox("Select a range for the graph", "Obtain Range Object", Type:=8)
    ActiveSheet.Shapes.AddChart2(201, xlColumnClustered).Select
    ActiveChart.SetSourceData Source:=rng1
    ActiveChart.PlotBy = xlRows
    
    'format area
    ActiveSheet.Shapes(ActiveChart.Parent.Name).Fill.Visible = msoFalse
    ActiveSheet.Shapes(ActiveChart.Parent.Name).Line.Visible = msoFalse
    ActiveChart.ChartTitle.Select
    Selection.Delete
    ActiveChart.Axes(xlValue).MajorGridlines.Select
    Selection.Delete
    With ActiveChart.ChartArea.Format.Line
        .Visible = msoFalse
    End With

    'format y-axis
    With ActiveChart.ChartArea.Format.TextFrame2.TextRange.Font
        .Name = "Helvetica"
        .Size = 8
        .Fill.ForeColor.RGB = RGB(0, 0, 0)
        .Fill.Transparency = 0
        .Fill.Solid
    End With
    ActiveChart.Axes(xlValue).MajorTickMark = xlOutside
    With ActiveChart.Axes(xlValue).Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorText1
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
        .Weight = 0.5
    End With

    'format x-axis
    ActiveChart.Axes(xlCategory).MajorTickMark = xlOutside
    ActiveChart.Axes(xlCategory).TickLabelPosition = xlNone
    With ActiveChart.Axes(xlCategory).Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorText1
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
        .Weight = 0.5
    End With
    
    ' count data series
    seriesCount = 0
    For Each fsc In ActiveChart.FullSeriesCollection
        seriesCount = seriesCount + 1
    Next fsc

    ' format data series: circular markers for all except last which is histogram
    i = 1
    For Each fsc In ActiveChart.FullSeriesCollection
        If i < (seriesCount - 1) Then
            With fsc
                .ChartType = xlLineMarkers
                .Format.Line.Visible = msoFalse
                .MarkerStyle = 8
                .MarkerSize = 3
                .Format.Fill.Visible = msoTrue
                .Format.Fill.ForeColor.RGB = RGB(0, 0, 0)
                .Format.Line.ForeColor.RGB = RGB(255, 255, 255)
                .MarkerBackgroundColor = RGB(0, 0, 0)
                .MarkerForegroundColor = RGB(255, 255, 255)
                .Format.Line.Weight = 0.25
            End With
        ElseIf i = (seriesCount - 1) Then
            With fsc
                .Format.Line.ForeColor.RGB = RGB(0, 0, 0)
                .Format.Fill.ForeColor.RGB = RGB(255, 255, 255)
                .Format.Line.Visible = msoTrue
                .Format.Line.Weight = 0.5
            End With
        Else
            With ActiveChart.FullSeriesCollection(i - 1)
                .HasErrorBars = True
                .ErrorBars.Format.Line.Visible = msoTrue
                .ErrorBars.Format.Line.ForeColor.ObjectThemeColor = msoThemeColorText1
                .ErrorBars.Format.Line.Weight = 0.5
                .ErrorBar Direction:=xlY, Include:=xlPlusValues, Type:=xlCustom, Amount:=ActiveChart.FullSeriesCollection(i).Values
            End With
            ActiveChart.SeriesCollection(i).Delete
        End If
        i = i + 1
    Next fsc
    ActiveChart.ChartGroups(1).Overlap = 0
    ActiveChart.ChartGroups(1).GapWidth = 10

End Sub




