README -*- mode: org -*-
* README 

** What is this repository for?

-> VB script to make the qPCR graphs for 4 groups of data. Saves a lot of time. Creates a graph with reasonable quality that you can edit then in PowerPoint

-> VB script to arrange images from apotome in ppt. Saves time

-> R code to produce xkcd-style graphs

-> v0.0.1

** Setup

-> For macros: create new macro in xls or ppt file and run it

-> Tested in MS Excel 2013/MS Powerpoint

-> R code tested in Win10

-> Tests: todo

