Sub cell()
    Dim sld As Slide
    Dim img As Shape
    Dim vcount As Integer
    Dim hcount As Integer

    For Each sld In ActivePresentation.Slides
        vcount = 30
        hcount = 10
        For Each img In sld.Shapes
            With img
                If .Type = msoLinkedPicture _
                Or .Type = msoPicture Then
                    'crop the image
                    .PictureFormat.CropLeft = 400
                    .PictureFormat.CropTop = 185
                    .PictureFormat.CropRight = 225
                    .PictureFormat.CropBottom = 355
                    'resize the image
                    .Height = 100
                    'check if the next column is needed
                    If vcount > 500 Then
                        vcount = 30
                        hcount = hcount + 320
                    End If
                    'position the image
                    .Left = hcount
                    .Top = vcount
                    
                    vcount = vcount + 102
                End If
            End With

        Next
    Next sld
End Sub
